﻿using Fluxor;
using MatBlazor;
using Newtonsoft.Json;
using Rudai.Things.Flux.Client.Store.Bookings.Actions;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client
{
    public partial class App : IDisposable
    {
        private readonly IStore Store;
        public readonly IDispatcher Dispatcher;
        private readonly IActionSubscriber ActionSubscriber;

        private MatTheme theme1 = new MatTheme()
        {
            Primary = MatThemeColors.Teal._500.Value,
            Secondary = MatThemeColors.BlueGrey._500.Value,

        };

        MatTheme theme2 = new MatTheme()
        {
            Primary = "green",
            Secondary = "orange"
        };
        public App()
        {

        }

        public App(IStore store, IDispatcher dispatcher, IActionSubscriber actionSubscriber)
        {
            Store = store;
            Dispatcher = dispatcher;
            ActionSubscriber = actionSubscriber;
            SubscribeToActions();
        }
        private void SubscribeToActions()
        {
            Console.WriteLine($"Subscribing to action {nameof(GetBookingsAction)}");
            ActionSubscriber.SubscribeToAction<GetBookingsAction>(this, action =>
            {
                // Show the object from the server in the console
                //string jsonToShowInConsole = JsonConvert.SerializeObject(action., Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                //Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(AddBookingAction)}");
            ActionSubscriber.SubscribeToAction<AddBookingAction>(this, action =>
            {
                // Show the object from the server in the console
                string jsonToShowInConsole = JsonConvert.SerializeObject(action.Booking, Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(AddBookingSuccessAction)}");
            ActionSubscriber.SubscribeToAction<AddBookingSuccessAction>(this, action =>
            {
                // Show the object from the server in the console
                string jsonToShowInConsole = JsonConvert.SerializeObject(action.Booking, Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(AddTourAction)}");
            ActionSubscriber.SubscribeToAction<AddTourAction>(this, action =>
            {
                // Show the object from the server in the console
                string jsonToShowInConsole = JsonConvert.SerializeObject(action.Tour, Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(AddTourSuccessAction)}");
            ActionSubscriber.SubscribeToAction<AddTourSuccessAction>(this, action =>
            {
                // Show the object from the server in the console
                string jsonToShowInConsole = JsonConvert.SerializeObject(action.Tour, Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(GetToursAction)}");
            ActionSubscriber.SubscribeToAction<GetToursAction>(this, action =>
            {
                // Show the object from the server in the console
                //string jsonToShowInConsole = JsonConvert.SerializeObject(action., Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                //Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(UpdateTourAction)}");
            ActionSubscriber.SubscribeToAction<UpdateTourAction>(this, action =>
            {
                // Show the object from the server in the console
                string jsonToShowInConsole = JsonConvert.SerializeObject(action.Tour, Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                Console.WriteLine(jsonToShowInConsole);
            });

            Console.WriteLine($"Subscribing to action {nameof(UpdateTourSuccessAction)}");
            ActionSubscriber.SubscribeToAction<UpdateTourSuccessAction>(this, action =>
            {
                // Show the object from the server in the console
                string jsonToShowInConsole = JsonConvert.SerializeObject(action.Tours, Formatting.Indented);
                Console.WriteLine("Action notification: " + action.GetType().Name);
                Console.WriteLine(jsonToShowInConsole);
            });
        }


        void IDisposable.Dispose()
        {
            // IMPORTANT: Unsubscribe to avoid memory leaks!
            ActionSubscriber.UnsubscribeFromAllActions(this);
        }
    }
}

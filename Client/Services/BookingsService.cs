﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Services
{
    public interface IBookingsService
	{
		Task<Booking[]> GetBookingsAsync();
		Task<Booking[]> AddBookingAsync(Booking booking);
		Task<Booking[]> UpdateBookingAsync(Booking booking);
	}
	public class BookingsService : IBookingsService
	{

		private static List<Booking> Bookings = new List<Booking>()
		{
			new Booking(Guid.NewGuid(),"Booking 10101", "Booking 1 Description", DateTime.Now.AddDays(5)),
			new Booking(Guid.NewGuid(),"Booking 21010", "Booking 2 Description", DateTime.Now.AddDays(4)),
			new Booking(Guid.NewGuid(),"Booking 31010", "Booking 3 Description", DateTime.Now.AddDays(3)),
			new Booking(Guid.NewGuid(),"Booking 421010", "Booking 4 Description", DateTime.Now.AddDays(2)),
			new Booking(Guid.NewGuid(),"Booking 521010", "Booking 5 Description", DateTime.Now.AddDays(9)),
			new Booking(Guid.NewGuid(),"Booking 621010", "Booking 6 Description", DateTime.Now.AddDays(8)),
			new Booking(Guid.NewGuid(),"Booking 721010", "Booking 7 Description", DateTime.Now.AddDays(7)),
			new Booking(Guid.NewGuid(),"Booking 821010", "Booking 8 Description", DateTime.Now.AddDays(11))
		};

		public async Task<Booking[]> GetBookingsAsync()
		{
			await Task.Delay(1000);
			return Bookings
				.ToArray();
		}
		public async Task<Booking[]> AddBookingAsync(Booking booking)
		{
			await Task.Delay(1000);
			Bookings.Add(booking);
			return Bookings
				.ToArray();
		}

		public async Task<Booking[]> UpdateBookingAsync(Booking booking)
		{
			await Task.Delay(10);
			var update = Bookings.Where(t => t.Id != booking.Id).ToList();
			update.Add(booking);
			return update.ToArray();
		}
	}
}

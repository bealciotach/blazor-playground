﻿using Fluxor;
using Microsoft.Extensions.Logging;
using Rudai.Things.Flux.Client.Store.Bookings.Actions;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using Rudai.Things.Flux.Client.Store.UI.Actions;
using Rudai.Things.Flux.Client.Store.WeatherForecasts.Actions;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Services
{
    public class StateFacade
    {
        private readonly ILogger<StateFacade> _logger;
        private readonly IDispatcher _dispatcher;

        public StateFacade(ILogger<StateFacade> logger, IDispatcher dispatcher) =>
            (_logger, _dispatcher) = (logger, dispatcher);

        #region Load methods

        public void LoadTours()
        {
            _logger.LogInformation("Issuing action to load tours...");
            _dispatcher.Dispatch(new GetToursAction());
        }

        public void LoadBookings()
        {
            _logger.LogInformation("Issuing action to load bookings...");
            _dispatcher.Dispatch(new GetBookingsAction());
        }

        public void LoadWeatherForecast()
        {
            _logger.LogInformation("Issuing action to load weather...");
            _dispatcher.Dispatch(new GetWeatherForcastsAction());
        }

        #endregion

        #region Bookings
        public void AddBooking(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to add tour...");
            Booking booking = new Booking(id, title, description, tourDate);
            _dispatcher.Dispatch(new AddBookingAction(booking));
            _logger.LogInformation("Issued action to add Booking...");

        }
        public void UpdateBooking(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to update booking...");
            Booking booking = new Booking(id, title, description, tourDate);
            _dispatcher.Dispatch(new UpdateBookingAction(booking));
        }

        public void SelectBooking(Booking booking)
        {
            _logger.LogInformation("Issuing action to select booking...");

            _dispatcher.Dispatch(new SelectBookingAction(booking));
        }

        #endregion

        #region Tours

        public void AddTour(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to add tour...");
            Tour tour = new Tour(id, title, description, tourDate);
            _dispatcher.Dispatch(new AddTourAction(tour));
            _logger.LogInformation("Issued action to add tour...");

        }

        public void UpdateTour(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to update tour...");
            Tour tour = new Tour(id, title, description, tourDate);
            _dispatcher.Dispatch(new UpdateTourAction(tour));
        }

        public void SelectTour(Tour tour)
        {
            _logger.LogInformation("Issuing action to select tour...");

            _dispatcher.Dispatch(new SelectTourAction(tour));
        }
        #endregion

        #region UI
        public void ToggleLeftDrawer(bool isLeftDrawerOpen)
        {
            _logger.LogInformation("Issuing action to select tour...");

            _dispatcher.Dispatch(new ToggleLeftDrawerAction(isLeftDrawerOpen));
        } 
        #endregion
    }
}

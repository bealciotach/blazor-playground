﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Flux.Client.Services;
using Rudai.Things.Flux.Client.Store.WeatherForecasts;
using Rudai.Things.Flux.Client.Store.WeatherForecasts.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Pages
{
    public partial class FetchData
    {

        [Inject]
        private StateFacade StateFacade { get; set; }
        [Inject]
        private IState<WeatherForecastsState> WeatherState { get; set; }

        [Inject]
        private IDispatcher Dispatcher { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            StateFacade.LoadWeatherForecast();
        }
    }
}

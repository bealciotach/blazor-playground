﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Flux.Client.Store.Counter;
using Rudai.Things.Flux.Client.Store.Counter.Actions;

namespace Rudai.Things.Flux.Client.Pages
{
    public partial class Counter
    {
        [Inject]
        private IState<CounterState> CounterState { get; set; }
        [Inject]
        public IDispatcher Dispatcher { get; set; }

        private void IncrementCount()
        {
            var action = new IncrementCounterAction();
            Dispatcher.Dispatch(action);
        }
        private void DecrementCount()
        {
            var action = new DecrementCounterAction();
            Dispatcher.Dispatch(action);
        }
    }
}

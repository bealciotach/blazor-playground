﻿using Fluxor;
using Rudai.Things.Flux.Client.Store.Counter.Actions;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Tours.Reducers
{
    public class ToursReducer
    {
        /// <summary>
        /// ReduceGetToursAction loads tours
        /// </summary>
        /// <param name="state">ToursState</param>
        /// <param name="action">GetToursAction</param>
        /// <returns>new ToursState</returns>
        [ReducerMethod]
        public static ToursState ReduceGetToursAction(ToursState state, GetToursAction action) =>
        new ToursState(isLoading: true, false, tours: null, errorMessage: null);
        /// <summary>
        /// ReduceGetToursActionSuccess handles load success
        /// </summary>
        /// <param name="state">ToursState</param>
        /// <param name="action">GetToursSuccessAction</param>
        /// <returns>ToursState</returns>
        [ReducerMethod]
        public static ToursState ReduceGetToursActionSuccess(ToursState state, GetToursSuccessAction action) => 
            new ToursState(isLoading: false, true, tours: action.Tours, null);

        [ReducerMethod]
        public static ToursState ReduceGetToursFailureAction(ToursState state, GetToursFailureAction action)
        {
            return new ToursState(false, true, null, null, action.ErrorMessage);
        }

        [ReducerMethod]
        public static ToursState ReduceAddTourAction(ToursState state, AddTourAction action) =>
            new ToursState(isLoading: true, isLoaded: false, tours: null, null);

        [ReducerMethod]
        public static ToursState ReduceAddTourSuccessAction(ToursState state, AddTourSuccessAction action)
        {
            // Grab a reference to the current todo list, or initialize one if we do not currently have any loaded
            var currentTodos = state.Tours is null ?
                new List<Tour>() :
                state.Tours.ToList();

            // Add the newly created todo to our list and sort by ID
            currentTodos.Add(action.Tour);
            currentTodos = currentTodos
                .OrderBy(t => t.Id)
                .ToList();

            return new ToursState(false, true, currentTodos, state.SelectedTour, null);
        }


        [ReducerMethod]
        public static ToursState ReduceAddTourFailureAction(ToursState state, AddTourFailureAction action)
        {
            var tours = state.Tours is null ?
                new List<Tour>() :
                state.Tours.ToList();

            tours = tours
                .OrderBy(t => t.Id)
                .ToList();

            return new ToursState(false, true, tours, state.SelectedTour, action.ErrorMessage);
        }

        [ReducerMethod]
        public static ToursState ReduceUpdateTourAction(ToursState state, UpdateTourAction action) => 
            new ToursState(isLoading: true, isLoaded: false, tours: state.Tours, null);

        [ReducerMethod]
        public static ToursState ReduceUpdateTourSuccessAction(ToursState state, UpdateTourSuccessAction action)
        {
            var tours = action.Tours is null ?
                new List<Tour>() :
                action.Tours.ToList();

            tours = tours
                .OrderBy(t => t.Id)
                .ToList();

            return new ToursState(false, true, tours, null);
        }

        [ReducerMethod]
        public static ToursState ReduceUpdateTourFailureAction(ToursState state, UpdateTourFailureAction action)
        {
            var tours = state.Tours is null ?
                new List<Tour>() :
                state.Tours.ToList();

            tours = tours
                .OrderBy(t => t.Id)
                .ToList();

            return new ToursState(false, true, tours, state.SelectedTour, action.ErrorMessage);
        }

        [ReducerMethod]
        public static ToursState ReduceSelectTourAction(ToursState state, SelectTourAction action)
        {
            var tours = state.Tours is null ?
                new List<Tour>() :
                state.Tours.ToList();

            tours = tours
                .OrderBy(t => t.Id)
                .ToList();

            return new ToursState(false, state.IsLoaded, tours, action.Tour, null);
        }

    }
}

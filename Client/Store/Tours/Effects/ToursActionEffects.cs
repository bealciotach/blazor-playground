﻿using Fluxor;
using Rudai.Things.Flux.Client.Services;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Tours.Effects
{
	public class ToursActionEffects
	{
		private readonly IToursService ToursService;
		public ToursActionEffects(IToursService toursService)
		{
			ToursService = toursService;
		}
		[EffectMethod]
		public async Task HandleGetToursAction(GetToursAction action, IDispatcher dispatcher)
		{
            try
            {
				var tours = await ToursService.GetToursAsync();
				dispatcher.Dispatch(new GetToursSuccessAction(tours));
			}
            catch (Exception ex)
            {

				dispatcher.Dispatch(new GetToursFailureAction(ex.Message));
			}
		}
		[EffectMethod]
		public async Task HandleAddTourAction(AddTourAction action, IDispatcher dispatcher)
		{
			try
			{
				var tours = await ToursService.AddTourAsync(action.Tour);
				dispatcher.Dispatch(new AddTourSuccessAction(action.Tour));
			}
			catch (Exception ex)
			{

				dispatcher.Dispatch(new AddTourFailureAction(ex.Message));
			}
		}
		[EffectMethod]
		public async Task HandleUpdateTourAction(UpdateTourAction action, IDispatcher dispatcher)
		{
			try
			{
				var tours = await ToursService.UpdateTourAsync(action.Tour);
				dispatcher.Dispatch(new UpdateTourSuccessAction(tours));
			}
			catch (Exception ex)
			{

				dispatcher.Dispatch(new UpdateTourFailureAction(ex.Message));
			}
		}
	}
}

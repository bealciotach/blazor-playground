﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Tours.Actions
{
    public class AddTourAction
    {
        public AddTourAction(Tour tour) =>
            Tour = tour;

        public Tour Tour { get; }
    }
}

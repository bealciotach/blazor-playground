﻿using Rudai.Things.Flux.Shared;
using Rudai.Things.Flux.Shared.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Tours.Actions
{
    public class UpdateTourFailureAction : FailureAction
    {
        public UpdateTourFailureAction(string message) 
            :base(message)
        { }
    }
}

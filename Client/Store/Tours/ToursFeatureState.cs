﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Tours
{
    public class ToursFeatureState : Feature<ToursState>
    {
        public override string GetName() => "Tours";
        protected override ToursState GetInitialState() =>
            new ToursState(isLoading: true, isLoaded: false, tours: null, errorMessage: null);
    }
}

﻿using Fluxor;
using Rudai.Things.Flux.Client.Store.Counter.Actions;
using Rudai.Things.Flux.Client.Store.UI.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.UI.Reducers
{
    public class UIReducer
    {
        [ReducerMethod]
        public static UIState ReduceOpenLeftDrawerAction(UIState state, OpenLeftDrawerAction action) =>
        new UIState(isLeftDrawerOpen: true);

        [ReducerMethod]
        public static UIState ReduceCloseLeftDrawerAction(UIState state, CloseLeftDrawerAction action) =>
            new UIState(isLeftDrawerOpen: false);

        [ReducerMethod]
        public static UIState ReduceToggleLeftDrawerAction(UIState state, ToggleLeftDrawerAction action) => 
            new UIState(isLeftDrawerOpen: !state.IsLeftDrawerOpen);

    }
}

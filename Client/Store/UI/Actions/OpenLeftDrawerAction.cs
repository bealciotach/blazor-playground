﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.UI.Actions
{
    public class OpenLeftDrawerAction
    {
        public OpenLeftDrawerAction(bool isLeftFrawerOpen) => IsOpen = isLeftFrawerOpen;

        public bool IsOpen { get; }
    }
}

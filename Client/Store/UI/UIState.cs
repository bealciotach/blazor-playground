﻿using Fluxor;
using MatBlazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.UI
{
    public class UIState
    {
        public List<MatTheme> themes = new List<MatTheme>()
        {
            new MatTheme() {
                Primary = MatThemeColors.Teal._500.Value,
                Secondary = MatThemeColors.BlueGrey._500.Value
            },
            new MatTheme() {
                Primary = MatThemeColors.Teal._500.Value,
                Secondary = MatThemeColors.BlueGrey._500.Value
            }
        };

        public bool IsLeftDrawerOpen { get; set; }
        public UIState(bool isLeftDrawerOpen)
        {
            IsLeftDrawerOpen = isLeftDrawerOpen;
        }
    }

    public class UIFeatureState : Feature<UIState>
    {
        public override string GetName() => "UI State";
        protected override UIState GetInitialState() =>
            new UIState(isLeftDrawerOpen: true);
    }
}

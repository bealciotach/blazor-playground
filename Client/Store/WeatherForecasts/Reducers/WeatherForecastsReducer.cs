﻿using Fluxor;
using Rudai.Things.Flux.Client.Store.Counter.Actions;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using Rudai.Things.Flux.Client.Store.WeatherForecasts;
using Rudai.Things.Flux.Client.Store.WeatherForecasts.Actions;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.WeatherForecasts.Reducers
{
    public class WeatherForecastsReducer
    {
        [ReducerMethod]
        public static WeatherForecastsState ReduceGetWeatherForcastsAction(WeatherForecastsState state, GetWeatherForcastsAction action) =>
        new WeatherForecastsState(isLoading: true, forecasts: null);

        [ReducerMethod]
        public static WeatherForecastsState ReduceGetToursActionSuccess(WeatherForecastsState state, GetWeatherForcastsSuccessAction action) => 
            new WeatherForecastsState(isLoading: false, forecasts: action.Forecasts);

    }
}

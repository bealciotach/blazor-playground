﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.WeatherForecasts.Actions
{
    public class GetWeatherForcastsSuccessAction
    {
        public IEnumerable<WeatherForecast> Forecasts { get; }
        public GetWeatherForcastsSuccessAction(IEnumerable<WeatherForecast> forecasts)
        {
            Forecasts = forecasts;
        }
    }
}

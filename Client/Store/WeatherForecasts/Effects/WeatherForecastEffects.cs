﻿using Fluxor;
using Rudai.Things.Flux.Client.Services;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using Rudai.Things.Flux.Client.Store.WeatherForecasts.Actions;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.WeatherForecasts.Effects
{
	public class WeatherForecastEffects
	{
		private readonly IWeatherForecastService WeatherForecastService;


		public WeatherForecastEffects(IWeatherForecastService weatherForecastService)
		{
			WeatherForecastService = weatherForecastService;
		}
		[EffectMethod]
		public async Task HandleGetWeatherForcastsAction(GetWeatherForcastsAction action, IDispatcher dispatcher)
		{
			var forecasts = await WeatherForecastService.GetForecastAsync(DateTime.Now);
			dispatcher.Dispatch(new GetWeatherForcastsSuccessAction(forecasts));
		}
	}
}

﻿using Fluxor;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings
{
    public class BookingsState
    {
        public bool IsLoading { get; }
        public bool IsLoaded { get; }
#nullable enable
        public string? CurrentErrorMessage { get; }
#nullable disable
        public IEnumerable<Booking> Bookings { get; }

        public Tour SelectedTour { get; }
        public Booking SelectedBooking { get; }

#nullable enable
        public BookingsState(bool isLoading, bool isLoaded, IEnumerable<Booking> bookings, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            Bookings = bookings ?? Array.Empty<Booking>();
            CurrentErrorMessage = errorMessage ?? string.Empty;
        }
        public BookingsState(bool isLoading, bool isLoaded, IEnumerable<Booking> bookings, Booking? selectedBooking, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            SelectedBooking = selectedBooking ?? null;
            Bookings = bookings ?? Array.Empty<Booking>();

            CurrentErrorMessage = errorMessage ?? string.Empty;
        }

#nullable disable
    }

}

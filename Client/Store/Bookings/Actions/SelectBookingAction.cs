﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Actions
{
    public class SelectBookingAction
    {
        public SelectBookingAction(Booking booking) => Booking = booking;

        public Booking Booking { get; }
    }
}

﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Actions
{
    public class AddBookingAction
    {
        public AddBookingAction(Booking booking) =>
            Booking = booking;

        public Booking Booking { get; }
    }
}

﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Actions
{
    public class AddBookingSuccessAction
    {
        public Booking Booking { get; }
        public AddBookingSuccessAction(Booking booking) =>
            Booking = booking;
    }
}

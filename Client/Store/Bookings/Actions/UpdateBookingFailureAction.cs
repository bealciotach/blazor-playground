﻿using Rudai.Things.Flux.Shared;
using Rudai.Things.Flux.Shared.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Actions
{
    public class UpdateBookingFailureAction : FailureAction
    {
        public UpdateBookingFailureAction(string message) 
            :base(message)
        { }
    }
}

﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Actions
{
    public class UpdateBookingSuccessAction
    {
        public Booking[] Bookings { get; }
        public UpdateBookingSuccessAction(Booking[] bookings) =>
            Bookings = bookings;
    }
}

﻿using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Actions
{
    public class GetBookingsSuccessAction
    {
        public IEnumerable<Booking> Bookings { get; }
        public GetBookingsSuccessAction(IEnumerable<Booking> bookings)
        {
            Bookings = bookings;
        }
    }
}

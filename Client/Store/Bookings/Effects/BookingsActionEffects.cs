﻿using Fluxor;
using Rudai.Things.Flux.Client.Services;
using Rudai.Things.Flux.Client.Store.Bookings.Actions;
using Rudai.Things.Flux.Client.Store.Tours.Actions;
using Rudai.Things.Flux.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings.Effects
{
	public class BookingsActionEffects
	{
		private readonly IBookingsService BookingsService;
		public BookingsActionEffects(IBookingsService bookingsService)
		{
			BookingsService = bookingsService;
		}
		[EffectMethod]
		public async Task HandleGetBookingsAction(GetBookingsAction action, IDispatcher dispatcher)
		{
            try
            {
				var bookings = await BookingsService.GetBookingsAsync();
				dispatcher.Dispatch(new GetBookingsSuccessAction(bookings));
			}
            catch (Exception ex)
            {

				dispatcher.Dispatch(new GetBookingsFailureAction(ex.Message));
			}
		}
		[EffectMethod]
		public async Task HandleAddBookingAction(AddBookingAction action, IDispatcher dispatcher)
		{
			try
			{
				var bookings = await BookingsService.AddBookingAsync(action.Booking);
				dispatcher.Dispatch(new AddBookingSuccessAction(action.Booking));
			}
			catch (Exception ex)
			{

				dispatcher.Dispatch(new AddBookingFailureAction(ex.Message));
			}
		}
		[EffectMethod]
		public async Task HandleUpdateBookingAction(UpdateBookingAction action, IDispatcher dispatcher)
		{
			try
			{
				var bookings = await BookingsService.UpdateBookingAsync(action.Booking);
				dispatcher.Dispatch(new UpdateBookingSuccessAction(bookings));
			}
			catch (Exception ex)
			{

				dispatcher.Dispatch(new UpdateBookingFailureAction(ex.Message));
			}
		}
	}
}

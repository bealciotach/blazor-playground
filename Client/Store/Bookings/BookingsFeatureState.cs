﻿using Fluxor;
using Rudai.Things.Flux.Client.Store.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Bookings
{
    public class BookingsFeatureState : Feature<BookingsState>
    {
        public override string GetName() => "Bookings";
        protected override BookingsState GetInitialState() =>
            new BookingsState(isLoading: true, isLoaded: false, bookings: null, errorMessage: null);
    }
}

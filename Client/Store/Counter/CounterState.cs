﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Counter
{
    public class CounterState
    {
        public int Count { get; }
        public CounterState(int count)
        {
            Count = count;

        }
    }

    public class CounterFeatureState : Feature<CounterState>
    {
        public override string GetName() => "Counter";
        protected override CounterState GetInitialState() =>
            new CounterState(count: 0);
    }
}

﻿using Fluxor;
using Rudai.Things.Flux.Client.Store.Counter.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Flux.Client.Store.Counter.Reducers
{
    public class CounterReducer
    {
        [ReducerMethod]
        public static CounterState ReduceDecrementCounterAction(CounterState state, DecrementCounterAction action) =>
        new CounterState(count: state.Count > 0 ? state.Count - 1 : state.Count);

        [ReducerMethod]
        public static CounterState ReduceIncrementCounterAction(CounterState state, IncrementCounterAction action) => 
            new CounterState(count: state.Count + 1);

    }
}

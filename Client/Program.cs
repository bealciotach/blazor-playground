using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Fluxor;
using Rudai.Things.Flux.Client.Services;

namespace Rudai.Things.Flux.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddScoped<IWeatherForecastService, WeatherForecastService>();
            builder.Services.AddScoped<IToursService, ToursService>();
            builder.Services.AddScoped<IBookingsService, BookingsService>();
            builder.Services.AddScoped<StateFacade>();
            builder.Services.AddFluxor(config =>
            {
                config.ScanAssemblies(typeof(Program).Assembly)
                .UseReduxDevTools();
            });

            await builder.Build().RunAsync();
        }
    }
}

**Blazor Playground**
Coming full circle back from React and Angular to C#, using some React/Angular patterns for the craic.

1. Blazor Client SPA
2. Material UI and Theming
3. Fluxor for State Management
4. IdentityServer4 integration
5. Unit Testing

---

## Getting started
in Client directory, dotnet watch run
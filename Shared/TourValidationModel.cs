﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Rudai.Things.Flux.Shared
{
    public class TourValidationModel
    {
        [Required(ErrorMessage = "ID associated with this tour is required")]
        public Guid Id { get; set; }

        [MinLength(10, ErrorMessage = "minimum of 10")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Your Tour must have a title")]
        public string Title { get; set; }

        [MinLength(10, ErrorMessage = "minimum of 10")]
        [Required(ErrorMessage = "Description of this tour is required")]
        public string Description { get; set; }


        [Required]
        [CustomValidation(typeof(TourValidationModel), nameof(RequiredDateTime))]
        public DateTime TourDate { get; set; }

        public static ValidationResult RequiredDateTime(DateTime value, ValidationContext vc)
        {
            return value > DateTime.MinValue
                ? ValidationResult.Success
                : new ValidationResult($"The {vc.MemberName} field is required.", new[] { vc.MemberName });
        }
    }
}
